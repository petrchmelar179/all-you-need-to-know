**Tel.** +420 720 406 366  
**Email** petrchmelar179@gmail.com  
**GitLab** https://gitlab.com/petrchmelar179/  
**LinkedIn** https://www.linkedin.com/in/petrchmelar/  
**This CV repository** https://gitlab.com/petrchmelar179/all-you-need-to-know/

# Petr Chmelař
*  Several years of experience as a software engineer in industry automation
*  Experiences with software development and design ranging from low-level control, business logic and data processing to user interface design
*  Several years of experience as a team leader of a team consisting 8 people

## Education
**Electrical, Electronic, Communication and Control Technology**  
*2016 - 2018*
*  Brno University of Technology - Faculty of Electrical Engineering and Communication
*  Master degree

**Electrical, Electronic, Communication and Control Technology**  
*2013 - 2016*
*  Brno University of Technology - Faculty of Electrical Engineering and Communication
*  Bachelor degree

## Skills
*  Programming languages - C#, Python, C++ and C
*  Architectural pattern - Model View ViewModel, Model View Update
*  Methodologies - Object-Oriented programming, Asynchronous programming, Functional programming, Reactive programming and Test Driven Development
*  Languages - English B2

## Tools
*  C# - dotnet core, dotnet framework, WPF, UWP, gRPC, xUnit, Reactive Extensions, Entity framework, ReactiveUI, OpenCVSharp, EmguCV, gRPC
*  Python - numpy, pandas, opencv, scikit-learn, PyTest, Flask
*  C++ - Embedded Template Library, CppUTest, cmake
*  Version control - Git, Mercurial, Gitlab and Github
*  Development automation - Gitlab CI
*  Others - Docker and PowerShell

## Experience
**ELEDUS / MICo Vision**  
Lead Developer  
*June 2018 - Present*
*  Development of software for X-Ray machine SCIOX
*  Development of software for industrial grade automatic X-Ray machines
*  Participating in team leadership
*  Software architecture design
*  Quality assurance

**ELEDUS**  
Software Engineer, Part-Time  
*August 2015 - May 2018*
*  Development of software for X-Ray machine SCIOX
*  Design and development of software for universal carriage
*  Design and development of a system for hydroponic growing

**METEL**  
Junior Firmware Engineer, Part-Time  
*April 2014 - December 2014*
*  Development of software for industrial grade network devices
