**Tel.** +420 720 406 366  
**Email** petrchmelar179@gmail.com  
**GitLab** https://gitlab.com/petrchmelar179/  
**LinkedIn** https://www.linkedin.com/in/petrchmelar/  
**Repozitář tohoto CV** https://gitlab.com/petrchmelar179/all-you-need-to-know/

# Petr Chmelař
*  Několikaleté zkušenosti s vývojem SW převážně v prostředí průmyslové automatizace, který je vyvíjen s důrazem na kvalitu kódu a vysokou míru automatizace vývoje
*  Zkušenosti s návrhem a vývojem software, od řízení na úrovni hardware, přes bussiness logiku a zpracování dat, po návrh grafického rozhraní
*  Několikaleté zkušenosti s vedením týmu o 5 až 8 vývojářích

## Dosažené vzdělání
**Kybernetika, automatizace a měření**  
*2016 - 2018*
*  Vysoké učení technické v Brně - Fakulta elektrotechniky a komunikačních technologií
*  Magisterský stupeň

**Automatizační a měřicí technika**  
*2013 - 2016*
*  Vysoké učení technické v Brně - Fakulta elektrotechniky a komunikačních technologií
*  Bakalářský stupeň

## Dovednosti
*  Programovací jazyky - C#, Python, C++ a C
*  Návrhové vzory - Model View Viewmodel, Model View Update
*  Metodiky vývoje - Objektově orientované programování, Funkcionální programování, Asynchronní programování, Reaktivní programování a Test Driven Development
*  Jazyky - Angličtina B2

## Nástroje
*  C# - dotnet core, dotnet framework, WPF, UWP, gRPC, xUnit, Reactive Extensions, Entity framework, ReactiveUI, OpenCVSharp, EmguCV, gRPC
*  Python - numpy, pandas, opencv, scikit-learn, PyTest, Flask
*  C++ - Embedded Template Library, CppUTest, cmake 
*  Verzovací nástroje - Git, Mercurial, Gitlab a Github
*  Automatizace vývoje - Gitlab CI
*  Ostatní - Docker a PowerShell

## Pracovní zkušenosti
**ELEDUS**  
Lead Developer  
*červen 2018 - Současnost*
*  Vývoj SW pro rentgenové zařízení SCIOX
*  Vývoj SW pro pro jednoúčelová rentgenová zařízení
*  Podílení se na vedení osmi členného teamu zodpovědného za vývoj SW pro rentgenové zařízení
*  Návrhování architektury
*  Zajišťování kvality

**ELEDUS / MICo Vision**  
Software Engineer, Part-Time  
*srpen 2015 - červen 2018*
*  Vývoj SW pro rentgenové zařízení SCIOX v jazyce C# (.NET Framework, .NET Core, WPF)
*  Návrh a vývoj SW pro elektrický víceúčelový vozík (C)
*  Návrh a vývoj SW pro systém pro hydroponické pěstování rostlin (C, Python, Kivy)

**METEL**  
Junior Firmware Engineer, Part-Time  
*duben 2014 - prosinec 2014*
*  Vývoj SW pro průmyslové síťové prvky (C)
