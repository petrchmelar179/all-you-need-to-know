# Decription
This repository contains sources to render my CV.
The CV is rendered using automatic pipeline using a Gitlab-CI.
Check out the latest release artifacts to get my CV rendered in PDF.

# Special thanks to
*  [Stephen Corwin](https://github.com/stephencorwin) and his resume, which is available on [Github](https://github.com/stephencorwin/resume) and which was great source of inspiration
*  [Raegon Kim](https://github.com/raycon) and his [vscode-markdown-style](https://github.com/raycon/vscode-markdown-style) which helped me to style my markdown
*  [Alan Shaw](https://github.com/alanshaw) and his [makrdown-pdf](https://github.com/alanshaw/markdown-pdf) which I'm using to render my markdown files
*  [Akihiro Uchida](https://github.com/uchida) and his [docker-markdown-pdf
](https://github.com/uchida/docker-markdown-pdf) which helped me to run markdown-pdf in a Docker on the Gitlab-CI